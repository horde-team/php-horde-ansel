#!/usr/bin/php
<?php
/**
 * This script removes references to files from package.xml
 */

function usage() {
  global $argv;
  echo "Usage:\n";
  echo "  php $argv[0] <package.xml> <filename1> [<filename2> [...]]\n";
  echo "\n";
  echo "  Remove references to <filename1>, <filename2>, ... from package.xml\n";
}

if (($argc < 3) || ($argv[1] == '-h') || ($argv[1] == '--help')) {
  usage();
  exit(1);
}

$args = $argv;
array_shift($args); // script name
$filePath = array_shift($args); // package.xml path
$dom = new DOMDocument();
$dom->load($filePath);
$domXPath = new DOMXPath($dom);
foreach($args as $nameToRemove) {
  $elementsToRemove = $domXPath->query("//*[@name='${nameToRemove}']");
  foreach($elementsToRemove as $elementToRemove) {
    $elementToRemove->parentNode->removeChild($elementToRemove);
  }
}
$dom->save($filePath);
